//
//  DownloadService.swift
//  URLSessionDemo
//
//  Created by Minh.Thang on 8/23/19.
//  Copyright © 2019 Minh.Thang. All rights reserved.
//

import Foundation

class DownloadService {
    
    static let shared = DownloadService()
    func setDownloadSession(delegate: URLSessionDownloadDelegate) {
        
        session.downloadTask(with: , completionHandler: <#T##(URL?, URLResponse?, Error?) -> Void#>)
        
        
    }
    let backgroundSession : URLSession = {
        let configuration = URLSessionConfiguration.background(withIdentifier:
            "com.bigzeroacademy.demoURLSession")
        return URLSession(configuration: configuration, delegate: delegate, delegateQueue: nil)
    }()
    
    func start(song: Song) {
        if (download.downloadedData == nil) {
            //start download
        } else {
            // resume download
        }
    }
    
    func pause() {
        
    }
    
}

// MARK: - <#Mark#>

extension Song {
    func download() {
        DownloadService.shared.start(song: self)
    }
    
    func pause() {
        
    }
}
