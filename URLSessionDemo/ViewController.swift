//
//  ViewController.swift
//  URLSessionDemo
//
//  Created by Minh.Thang on 8/22/19.
//  Copyright © 2019 Minh.Thang. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UISearchResultsUpdating {
    
    let searchController = UISearchController(searchResultsController: SearchResultController.instance )
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.searchController.searchResultsUpdater = self
        self.searchController.delegate = self
        self.searchController.searchBar.delegate = self
        
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.searchController.dimsBackgroundDuringPresentation = true
        self.navigationItem.titleView = searchController.searchBar
        self.definesPresentationContext = true
        
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        guard let queryText = searchController.searchBar.text else {return }
        SearchQuery.shared.getSong(searchText: queryText) { (songs) in
            let searchResultController = (searchController.searchResultsController as! SearchResultController)
            searchResultController.listSong = songs
        }
    }


}


// MARK: - <#Mark#>

extension ViewController:  UISearchControllerDelegate {
    
}


// MARK: - <#Mark#>

extension ViewController: UISearchBarDelegate {
    
}
