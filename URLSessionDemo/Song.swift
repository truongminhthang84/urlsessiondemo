//
//  Song.swift
//  URLSessionDemo
//
//  Created by Minh.Thang on 8/22/19.
//  Copyright © 2019 Minh.Thang. All rights reserved.
//

import Foundation

class Song {
    var url: URL
    var download 
    static func getList(rawValue: RawData) -> [Song] {
        var songs : [Song] = []
        for item in rawValue.results {
            if let song = Song(songRawValue: item) {
                songs.append(song)
            }
        }
        return songs
    }
    
    init?(songRawValue: RawData.SongRawData) {
        if let url = URL(string: songRawValue.previewUrl) {
            self.url = url
        } else {
            return nil
        }
        
    }
}

class RawData: Decodable {
    var results: [SongRawData] = []
    
    class SongRawData: Decodable {
        var artistName: String
        var previewUrl: String
    }
}
