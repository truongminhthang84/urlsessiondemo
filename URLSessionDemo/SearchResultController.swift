//
//  SearchResultController.swift
//  URLSessionDemo
//
//  Created by Minh.Thang on 8/22/19.
//  Copyright © 2019 Minh.Thang. All rights reserved.
//

import UIKit

class SearchResultController: UITableViewController {
    
    
    static var instance : SearchResultController  = {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "SearchResultController") as! SearchResultController
    }()
    var listSong: [Song] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listSong.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = listSong[indexPath.row].url.absoluteString
        return cell
    }
}
