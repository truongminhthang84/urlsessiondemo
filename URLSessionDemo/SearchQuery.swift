//
//  SearchQuery.swift
//  URLSessionDemo
//
//  Created by Minh.Thang on 8/22/19.
//  Copyright © 2019 Minh.Thang. All rights reserved.
//

import Foundation
// Query
//) {
//    urlComponents.query = "\(searchTerm)"
class SearchQuery {
    static let shared = SearchQuery()
    func getSong(searchText: String, completeHandler: @escaping ([Song]) -> ()) {
        guard let url = URL(string: "https://itunes.apple.com/search/media=music&entity=song&term=\(searchText)") else {return }
        let dataTask = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard error == nil else {return}
            guard (response as! HTTPURLResponse).statusCode == 200 else {return}
            guard let data = data else {return}
            do {
                let songRawData = try JSONDecoder().decode(RawData.self, from: data)
                let songs = Song.getList(rawValue: songRawData)
                DispatchQueue.main.async {
                    completeHandler(songs)
                }
                
            } catch {
                print(error.localizedDescription)
            }
        }
        dataTask.resume()

    }
}
